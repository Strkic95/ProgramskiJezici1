#pragma once
#include <string>
#include <iostream>
#include <functional>
#include <iterator>

class LinkedList
{
	struct Node
	{
		std::string data;
		Node* next, *prev;
		Node(const std::string&);
	};
	friend std::ostream& operator<<(std::ostream&, const LinkedList&);
public:
	class LinkedListIterator : public std::iterator<std::forward_iterator_tag, Node*>
	{
		Node* itr;
		explicit LinkedListIterator(Node*);
		friend class LinkedList;
	public:
		LinkedListIterator() : itr(nullptr) {}
		void swap(LinkedListIterator& other);
		LinkedListIterator& operator++();
		LinkedListIterator operator++(int);
		bool operator==(const LinkedListIterator&) const;
		bool operator!=(const LinkedListIterator&) const;
		std::string& operator*() const;
		std::string& operator->() const;
	};
	LinkedList();
	LinkedList(const LinkedList&);
	LinkedList(LinkedList&&);
	~LinkedList();
	LinkedList& operator=(const LinkedList&);
	LinkedList& operator=(LinkedList&&);
	void add(const std::string&);
	void removeAt(int);
	int size();
	LinkedList filter(std::function<bool(const std::string&)>) const;
	std::string operator[](int) const;
	bool operator==(const LinkedList&) const;
	bool operator!=(const LinkedList&) const;
	LinkedListIterator begin();
	LinkedListIterator end();
private:
	inline bool isIndexInRange(int) const;
	Node* getElementAt(int) const;
	void copy(const LinkedList&);
	inline void move(LinkedList&&);
	Node* head;
	Node* tail;
	int n;
};


