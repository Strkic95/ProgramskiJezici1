#include "LinkedList.h"

LinkedList::Node::Node(const std::string& str) : next(nullptr), prev(nullptr), data(str) {}

LinkedList::LinkedList() : head(nullptr), tail(nullptr), n(0) {}

LinkedList::LinkedList(const LinkedList& other)
{
	copy(other);
}

LinkedList::LinkedList(LinkedList&& other)
{
	move(std::move(other));
}

LinkedList::~LinkedList()
{
	Node* temp = head;
	while (temp)
	{
		Node* toDelete = temp;
		temp = temp->next;
		delete toDelete;
	}
	head = tail = nullptr;
	n = 0;
}

LinkedList& LinkedList::operator=(const LinkedList& other)
{
	if (this == &other)
		return *this;
	copy(other);
	return *this;
}

LinkedList& LinkedList::operator=(LinkedList&& other)
{
	move(std::move(other)); //move(static_cast<LinkedList&&>(other));
	return *this;
}

void LinkedList::add(const std::string& str)
{
	Node* temp = new Node(str);
	if (head)
	{
		temp->next = head;
		head->prev = temp;
		head = temp;
	}
	else
		head = tail = temp;
	++n;
}

void LinkedList::removeAt(int i)
{
	if (!isIndexInRange(i))
		return;
	Node* node = getElementAt(i);
	node->prev->next = node->next;
	node->next->prev = node->prev;
	delete node;
	--n;
}

int LinkedList::size()
{
	return n;
}

LinkedList LinkedList::filter(std::function<bool(const std::string&)> f) const
{
	LinkedList temp;
	for (Node* node = tail; node; node = node->prev)
		if (f(node->data))
			temp.add(node->data);
	return temp;
}

std::string LinkedList::operator[](int i) const
{
	if (!isIndexInRange(i))
		return std::string();
	return getElementAt(i)->data;
}

bool LinkedList::operator==(const LinkedList& other) const
{
	if (n != other.n)
		return false;
	for (Node* node1 = head, *node2 = other.head; node1; node1 = node1->next, node2 = node2->next)
		if (node1->data != node2->data)
			return false;
	return true;
}

bool LinkedList::operator!=(const LinkedList& other) const
{
	return !(*this == other);
}

LinkedList::LinkedListIterator LinkedList::begin()
{
	return LinkedListIterator(head);
}

LinkedList::LinkedListIterator LinkedList::end()
{
	return LinkedListIterator();
}

inline bool LinkedList::isIndexInRange(int i) const
{
	return i >= 0 && i < n;
}

LinkedList::Node* LinkedList::getElementAt(int i) const
{
	Node* node;
	for (node = head; i; node = node->next, --i);
	return node;
}

void LinkedList::copy(const LinkedList& other)
{
	LinkedList::~LinkedList();
	for (Node* node = other.tail; node; node = node->prev)
		add(node->data);
}

void LinkedList::move(LinkedList&& other)
{
	head = other.head;
	tail = other.tail;
	n = other.n;
	other.head = other.tail = nullptr;
}

std::ostream& operator<<(std::ostream& str, const LinkedList& list)
{
	for (LinkedList::Node* node = list.head; node; node = node->next)
		str << node->data << std::endl;
	return str;
}

LinkedList::LinkedListIterator::LinkedListIterator(Node* node) : itr(node) {}

inline void LinkedList::LinkedListIterator::swap(LinkedListIterator & other)
{
	std::swap(itr, other.itr);
}

LinkedList::LinkedListIterator& LinkedList::LinkedListIterator::operator++()
{
	itr = itr->next;
	return *this;
}

LinkedList::LinkedListIterator LinkedList::LinkedListIterator::operator++(int)
{
	LinkedListIterator tmp(*this);
	itr = itr->next;
	return tmp;
}

bool LinkedList::LinkedListIterator::operator==(const LinkedListIterator & rhs) const
{
	return itr == rhs.itr;
}

bool LinkedList::LinkedListIterator::operator!=(const LinkedListIterator & rhs) const
{
	return itr != rhs.itr;
}

std::string & LinkedList::LinkedListIterator::operator*() const
{
	return itr->data;
}

std::string & LinkedList::LinkedListIterator::operator->() const
{
	return itr->data;
}
