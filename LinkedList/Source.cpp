#include "LinkedList.h"
using std::cout;
using std::endl;

LinkedList createTemp()
{
	LinkedList temp;
	temp.add("temp");
	return temp;
}

int main()
{
	LinkedList list, temp;
	temp.add("Temporary");
	list.add("Blah");
	list.add("Box");
	list.add("Baroque");
	list.add("Whatever");
	LinkedList copy = list;
	cout << "List:" << endl;
	cout << list << endl;
	cout << "Comparison:" << endl;
	cout << (list == copy) << endl;
	cout << (list == temp) << endl << endl;
	cout << "Temporary:" << endl;
	cout << createTemp()[0] << endl << endl;
	cout << "Filtered:" << endl;
	cout << list.filter([](const std::string& str) {return str[0] == 'B';}) << endl;
	cout << "Iterator:" << endl;
	for (auto& it : list)
		cout << it << endl;
}