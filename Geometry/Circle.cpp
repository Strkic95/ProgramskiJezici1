#define _USE_MATH_DEFINES
#include "Circle.h"
#include <cmath>
#define TWO_PI (2*M_PI)

Circle::Circle(const std::string& name, double r) : Shape2D(name), r(r) {}

double Circle::circ() const
{
	return TWO_PI * r;
}

double Circle::surf() const
{
	return r * r * M_PI;
}

void Circle::print(std::ostream& str) const
{
	Shape2D::print(str);
	str << " (r=" << r << ")";
}

void Circle::read(std::istream& str)
{
	Shape2D::read(str);
	str >> r;
}
