#pragma once
#include "Shape2D.h"
#include <initializer_list>

struct Point2D
{
	double x = 0;
	double y = 0;
	Point2D(double = 0, double = 0);
	double distance(const Point2D&);
};

class Polygon : public Shape2D
{
public:
	Polygon(const std::string&, unsigned);
	Polygon(const std::string&, const std::initializer_list<Point2D>&);
	Polygon(const Polygon&);
	Polygon(Polygon&&);
	~Polygon();
	Polygon& operator=(const Polygon&);
	Polygon& operator=(Polygon&&);
	virtual double circ() const override;
	virtual void print(std::ostream&) const override;
	virtual void read(std::istream&) override;
	Point2D& operator[](unsigned);
private:
	static std::ostream& printPoint(std::ostream&, const Point2D&);
	static std::istream& readPoint(std::istream&, Point2D&);
	void copy(const Polygon&);
	void move(Polygon&&);
	Point2D* points;
	unsigned n;
};