#pragma once
#include "IReadable.h"
#include "IPrintable.h"
#include <string>

class Shape2D : virtual public IReadable, virtual public IPrintable
{
public:
	Shape2D(const std::string&);
	virtual double circ() const = 0;
	virtual ~Shape2D();
protected:
	virtual void print(std::ostream&) const override;
	virtual void read(std::istream&) override;
private:
	std::string name;
};