#pragma once
#include "Polygon.h"
#include "ISurface.h"

class Rectangle : public Shape2D, virtual public ISurface
{
public:
	Rectangle(const std::string&, double, double);
	virtual double circ() const override;
	virtual double surf() const override;
private:
	double a, b;
};