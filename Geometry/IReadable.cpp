#include "IReadable.h"

std::istream& operator >> (std::istream& str, IReadable& readable)
{
	readable.read(str);
	return str;
}
