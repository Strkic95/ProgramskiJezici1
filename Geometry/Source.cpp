#include "Circle.h"
#include "Polygon.h"
using std::cout;
using std::endl;

int main()
{
	Circle c("Circle", 2.0);
	Polygon p("Polygon", 4);
	const Polygon pp("Initialized Polygon", { Point2D(1,1), Point2D(2,2) });
	ISurface& surf = c;
	cout << surf.surf() << endl;
	cout << c << endl;
	cout << p << endl;
	Shape2D& cr = c;
	cout << cr << endl;
	Shape2D* shapeArr[] = { &c, &p };
	for (Shape2D* shape : shapeArr)
		cout << *shape << endl;
	const IPrintable* printArr[] = { &c, &p, &pp };
	for (const IPrintable* printable : printArr)
		cout << *printable << endl;
	cout << shapeArr[0]->circ() << endl;
	cout << shapeArr[1]->circ() << endl;
	Shape2D* dynPol = new Polygon("Dynamic polygon", 4);
	(*((Polygon*)dynPol))[0].x = 1.0; //Don't!
	cout << *dynPol;
	delete dynPol;
}