#pragma once

struct ISurface
{
	virtual double surf() const = 0;
};