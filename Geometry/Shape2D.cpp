#include "Shape2D.h"

Shape2D::Shape2D(const std::string& name) : name(name) {}

Shape2D::~Shape2D() {}

void Shape2D::print(std::ostream& str) const
{
	str << name;
}

void Shape2D::read(std::istream& str)
{
	str >> name;
}
