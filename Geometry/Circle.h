#pragma once
#include "Shape2D.h"
#include "ISurface.h"

class Circle : public Shape2D, virtual public ISurface
{
public:
	Circle(const std::string&, double r);
	virtual double circ() const override;
	virtual double surf() const override;
	virtual void print(std::ostream&) const override;
	virtual void read(std::istream&) override;
private:
	double r;
};