#include "IPrintable.h"

std::ostream& operator<<(std::ostream& str, const IPrintable& printable)
{
	printable.print(str);
	return str;
}
