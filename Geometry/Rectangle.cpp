#include "Rectangle.h"

Rectangle::Rectangle(const std::string& name, double a, double b) : Shape2D(name), a(a), b(b) {}

double Rectangle::circ() const
{
	return 2 * (a + b);
}

double Rectangle::surf() const
{
	return a*b;
}
