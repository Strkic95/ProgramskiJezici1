#include "Polygon.h"
#include <algorithm>

Polygon::Polygon(const std::string& name, unsigned n) : Shape2D(name), n(n)
{
	points = new Point2D[n];
}

Polygon::Polygon(const std::string& name, const std::initializer_list<Point2D>& list) : Polygon(name, list.size())
{
	std::copy(list.begin(), list.end(), points);
}

Polygon::Polygon(const Polygon& other) : Shape2D(other)
{
	copy(other);
}

Polygon::Polygon(Polygon&& other) : Shape2D(std::move(other))
{
	move(std::move(other));
}

Polygon::~Polygon()
{
	delete[] points;
}

Polygon& Polygon::operator=(const Polygon& other)
{
	if (this != &other)
	{
		Polygon::~Polygon();
		copy(other);
	}
	return *this;
}

Polygon& Polygon::operator=(Polygon&& other)
{
	move(std::move(other));
	return *this;
}

double Polygon::circ() const
{
	double result = points[0].distance(points[n - 1]);
	for (unsigned i = 0; i < n - 1; ++i)
		result += points[i].distance(points[i + 1]);
	return result;
}

void Polygon::print(std::ostream& str) const
{
	Shape2D::print(str);
	str << ": ";
	std::for_each(points, points + n, [&str](const Point2D& point) { printPoint(str, point);});
}

void Polygon::read(std::istream& str)
{
	Shape2D::read(str);
	std::for_each(points, points + n, [&str](Point2D& point) { readPoint(str, point);});
}

Point2D& Polygon::operator[](unsigned i)
{
	return points[i];
}

std::ostream& Polygon::printPoint(std::ostream& str, const Point2D& point)
{
	return str << "(" << point.x << ", " << point.y << ")";
}

std::istream& Polygon::readPoint(std::istream& str, Point2D& point)
{
	return str >> point.x >> point.y;
}

void Polygon::copy(const Polygon& other)
{
	points = new Point2D[n = other.n];
	std::copy(other.points, other.points + n, points);
}

void Polygon::move(Polygon&& other)
{
	points = other.points;
	n = other.n;
	other.points = nullptr;
}

Point2D::Point2D(double x, double y) : x(x), y(y) {}

double Point2D::distance(const Point2D& other)
{
	double dx = x - other.x;
	double dy = y - other.y;
	return std::sqrt(dx*dx + dy*dy);
}
