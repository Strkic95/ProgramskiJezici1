#pragma once
#include "BinaryTree.h"

template <class T, class comparer = std::less<T>>
class AVLTree : public BinaryTree<T, comparer>
{
protected:
	class AVLNode : public Node
	{
	public:
		AVLNode(T* const) noexcept;
		AVLNode(const AVLNode&) noexcept;
		int balanceFactor() const;
		virtual void insert(T&) override;
	protected:
		unsigned height;
		virtual Node* createNode(T&) override;
		virtual Node* copyNode(const Node&) override;
		void fixHeight();
		void rotateRight();
		void rotateLeft();
		void balance();
		constexpr unsigned getHeight() const;
	};
public:
	virtual Node* createNode(T&) override;
	virtual Node* copyNode(const Node&) override;
};

