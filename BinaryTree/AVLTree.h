#include "AVLTreeHeader.h"

template <class T, class comparer>
AVLTree<T, comparer>::AVLNode::AVLNode(T* const data) noexcept : Node(data) {}

template <class T, class comparer>
AVLTree<T, comparer>::AVLNode::AVLNode(const AVLNode& node) noexcept : Node(node), height(node.height) {}

template <class T, class comparer>
int AVLTree<T, comparer>::AVLNode::balanceFactor() const
{
	if (this == nullptr)
		return 0;
	return ((AVLNode*)right)->getHeight() - ((AVLNode*)left)->getHeight();
}

template <class T, class comparer>
void AVLTree<T, comparer>::AVLNode::insert(T& data)
{
	Node::insert(data);
	balance();
}

template <class T, class comparer>
typename AVLTree<T, comparer>::Node* AVLTree<T, comparer>::AVLNode::createNode(T& data)
{
	return new AVLNode(&data);
}

template <class T, class comparer>
typename AVLTree<T, comparer>::Node* AVLTree<T, comparer>::AVLNode::copyNode(const Node& node)
{
	return new AVLNode(*(AVLNode*)&node);
}

template <class T, class comparer>
void AVLTree<T, comparer>::AVLNode::fixHeight()
{
	unsigned hl = ((AVLNode*)left)->getHeight();
	unsigned hr = ((AVLNode*)right)->getHeight();
	height = (hl > hr ? hl : hr) + 1;
}

template <class T, class comparer>
void AVLTree<T, comparer>::AVLNode::rotateRight()
{
	AVLNode* a = (AVLNode*)((AVLNode*)left)->left;
	AVLNode* b = (AVLNode*)((AVLNode*)left)->right;
	AVLNode* c = (AVLNode*)right;
	T* temp = ((AVLNode*)left)->data;
	((AVLNode*)left)->data = data;
	data = temp;
	right = left;
	left = a;
	((AVLNode*)right)->left = b;
	((AVLNode*)right)->right = c;
}

template <class T, class comparer>
void AVLTree<T, comparer>::AVLNode::rotateLeft()
{
	AVLNode* a = (AVLNode*)((AVLNode*)right)->right;
	AVLNode* b = (AVLNode*)((AVLNode*)right)->left;
	AVLNode* c = (AVLNode*)left;
	T* temp = ((AVLNode*)right)->data;
	((AVLNode*)right)->data = data;
	data = temp;
	left = right;
	right = a;
	((AVLNode*)left)->right = b;
	((AVLNode*)left)->left = c;
}

template <class T, class comparer>
void AVLTree<T, comparer>::AVLNode::balance()
{
	fixHeight();
	if (balanceFactor() == 2)
	{
		if (((AVLNode*)right)->balanceFactor() < 0)
			((AVLNode*)right)->rotateRight();
		rotateLeft();
	}
	if (balanceFactor() == -2)
	{
		if (((AVLNode*)left)->balanceFactor() > 0)
			((AVLNode*)left)->rotateLeft();
		rotateRight();
	}
}

template <class T, class comparer>
constexpr unsigned AVLTree<T, comparer>::AVLNode::getHeight() const
{
	return this != nullptr ? height : 0;
}

template <class T, class comparer>
typename AVLTree<T, comparer>::Node* AVLTree<T, comparer>::createNode(T& data)
{
	return new AVLNode(&data);
}

template <class T, class comparer>
typename AVLTree<T, comparer>::Node* AVLTree<T, comparer>::copyNode(const Node& node)
{
	return new AVLNode(*(AVLNode*)&node);
}
