#pragma once
#include <string>
class Person
{
	friend std::ostream& operator<<(std::ostream&, const Person&);
	int index;
	std::string name;
public:
	Person(int, char*);
	int getIndex() const;
	const std::string& getName() const;
	bool operator<(const Person&) const;
};

