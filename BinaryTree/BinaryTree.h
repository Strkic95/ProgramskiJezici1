#include "BinaryTreeHeader.h"
#include "Exception.h"

template <class T, class comparer>
BinaryTree<T, comparer>::BinaryTree() noexcept : root(nullptr) {}

template <class T, class comparer>
BinaryTree<T, comparer>::BinaryTree(const BinaryTree<T, comparer>& other) noexcept : root(new Node(*other.root)) {}

template <class T, class comparer>
BinaryTree<T, comparer>::BinaryTree(BinaryTree<T, comparer>&& other) noexcept : root(std::move(other.root))
{
	other.root = nullptr;
}

template <class T, class comparer>
BinaryTree<T, comparer>& BinaryTree<T, comparer>::operator=(const BinaryTree<T, comparer>& other) noexcept
{
	delete root;
	root = copyNode(*other.root);
	return *this;
}

template <class T, class comparer>
BinaryTree<T, comparer>& BinaryTree<T, comparer>::operator=(BinaryTree<T, comparer>&& other) noexcept
{
	delete root;
	root = std::move(other.root);
	return *this;
}

template <class T, class comparer>
void BinaryTree<T, comparer>::insert(T& data)
{
	if (root)
		root->insert(data);
	else
		root = createNode(data);
}

template<class T, class comparer>
const T& BinaryTree<T, comparer>::operator[](int i) const noexcept(false)
{
	if (i < 0) throw IndexOutOfRangeException("Index out of range.", i);
	int dummy = 0;
	T* temp = root->traverseToN(i, dummy);
	if (!temp) throw IndexOutOfRangeException("Index out of range.", i);
	return *temp;
}

template <class T, class comparer>
void BinaryTree<T, comparer>::IndexOutOfRangeException::print(std::ostream& str) const noexcept
{
	Exception::print(str);
	str << std::endl << "Index " << index << " does not exist.";
}

template <class T, class comparer>
typename BinaryTree<T, comparer>::Node* BinaryTree<T, comparer>::createNode(T& data)
{
	return new Node(&data);
}

template <class T, class comparer>
typename BinaryTree<T, comparer>::Node* BinaryTree<T, comparer>::copyNode(typename const BinaryTree<T, comparer>::Node& node)
{
	return new BinaryTree<T, comparer>::Node(node);
}

template <class T, class comparer>
BinaryTree<T, comparer>::~BinaryTree()
{
	delete root;
}

template <class T, class comparer>
BinaryTree<T, comparer>::Node::Node(T* const data, comparer comp) noexcept : data(data), left(nullptr), right(nullptr), comp(comp) {}

template <class T, class comparer>
BinaryTree<T, comparer>::Node::Node(typename const BinaryTree<T, comparer>::Node& other) noexcept : comp(other.comp)
{
	copy(other);
}

template <class T, class comparer>
typename BinaryTree<T, comparer>::Node& BinaryTree<T, comparer>::Node::operator=(typename const BinaryTree<T, comparer>::Node& other)
{
	this->~Node();
	copy(other);
	return *this;
}

template <class T, class comparer>
BinaryTree<T, comparer>::Node::~Node()
{
	if (left)
		delete left;
	if (right)
		delete right;
}

template <class T, class comparer>
void BinaryTree<T, comparer>::Node::insert(T& data)
{
	if (comp(data, *this->data))
		if (this->left)
			this->left->insert(data);
		else
			this->left = createNode(data);
	else
		if (this->right)
			this->right->insert(data);
		else
			this->right = createNode(data);
}

template <class T, class comparer>
void BinaryTree<T, comparer>::Node::print(std::ostream& str)
{
	if (left)
		left->print(str);
	str << *data << std::endl;
	if (right)
		right->print(str);
}

template<class T, class comparer>
T* BinaryTree<T, comparer>::Node::traverseToN(int n, int& c)
{
	if (left)
	{
		T* temp = left->traverseToN(n, c);
		if (temp)
			return temp;
	}
	if (c++ == n)
		return data;
	if (right)
	{
		T* temp = right->traverseToN(n, c);
		if (temp)
			return temp;
	}
	return nullptr;
}

template <class T, class comparer>
typename BinaryTree<T, comparer>::Node* BinaryTree<T, comparer>::Node::createNode(T& data)
{
	return new Node(&data);
}

template <class T, class comparer>
typename BinaryTree<T, comparer>::Node* BinaryTree<T, comparer>::Node::copyNode(const Node& node)
{
	return new Node(node);
}

template <class T, class comparer>
void BinaryTree<T, comparer>::Node::copy(const Node& other)
{
	data = other.data;
	left = other.left ? copyNode(*other.left) : nullptr;
	right = other.right ? copyNode(*other.right) : nullptr;
}

template <class T, class comparer>
std::ostream& operator<<(std::ostream& str, const BinaryTree<T, comparer>& tree)
{
	if (tree.root)
		tree.root->print(str);
	return str;
}
