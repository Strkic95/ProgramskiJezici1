#pragma once
#include <iostream>
#include <functional>

template <class T, class comparer = std::less<T>>
class BinaryTree
{
	template <class A, class B>
	friend std::ostream& operator<<(std::ostream&, const BinaryTree<A, B>&);
protected:
	class Node
	{
	public:
		Node(T* const, comparer = comparer()) noexcept;
		Node(const Node&) noexcept;
		Node& operator=(const Node&);
		virtual ~Node();
		virtual void insert(T&);
		virtual void print(std::ostream&);
		virtual T* traverseToN(int, int&);
	protected:
		virtual Node* createNode(T&);
		virtual Node* copyNode(const Node&);
		Node* left, *right;
		T* data;
	private:
		void copy(const Node&);
		comparer comp;
	};
	class IndexOutOfRangeException :public Exception
	{
		int index;
	public:
		IndexOutOfRangeException(char*, int) noexcept;
	protected:
		virtual void print(std::ostream&) const noexcept override;
	};
	Node* root;
	virtual Node* createNode(T&);
	virtual Node* copyNode(const Node&);
public:
	BinaryTree() noexcept;
	BinaryTree(const BinaryTree&) noexcept;
	BinaryTree(BinaryTree&&) noexcept;
	BinaryTree& operator=(const BinaryTree&) noexcept;
	BinaryTree& operator=(BinaryTree&&) noexcept;
	virtual ~BinaryTree();
	virtual void insert(T&);
	const T& operator[](int) const noexcept(false);
};

template<class T, class comparer>
BinaryTree<T, comparer>::IndexOutOfRangeException::IndexOutOfRangeException(char* message, int index) noexcept : Exception(message), index(index) {}
