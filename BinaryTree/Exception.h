#pragma once
#include <string>

class Exception
{
	friend std::ostream& operator<<(std::ostream&, const Exception&);
	std::string message;
public:
	Exception(char*) noexcept;
protected:
	virtual void print(std::ostream&) const noexcept;
};

