#include "Person.h"
#include "AVLTree.h"
#include <functional>
#include <ctime>
using std::cout;
using std::endl;

#define ITERATIONS 5000

Person a(10, "Arko Arkovic");
Person b(11, "Stanko Stankovic");
Person c(7, "Branko Brankovic");
Person d(15, "Pero Peric");

clock_t start;

template <>
struct std::less<Person>
{
	virtual bool operator()(const Person& a, const Person& b)
	{
		return a.getIndex() < b.getIndex();
	}
};

struct nameLess : public std::less<Person>
{
	bool operator()(const Person& a, const Person& b) override final
	{
		return a.getName() < b.getName();
	}
};

BinaryTree<Person> f()
{
	BinaryTree<Person> bt;
	for (int i = 0; i < ITERATIONS; ++i)
		bt.insert(a);
	start = clock();
	return bt;
}

AVLTree<Person> g()
{
	AVLTree<Person> bt;
	for (int i = 0; i < ITERATIONS; ++i)
		bt.insert(a);
	start = clock();
	return bt;
}

void measureTimeBinary()
{
	BinaryTree<Person> temp = f();
	clock_t end = clock();
	double elapsed_secs = double(end - start) * 1000 / CLOCKS_PER_SEC;
	cout << elapsed_secs << "ms" << endl;
}

void measureTimeAVL()
{
	AVLTree<Person> temp = g();
	clock_t end = clock();
	double elapsed_secs = double(end - start) * 1000 / CLOCKS_PER_SEC;
	cout << elapsed_secs << "ms" << endl;
}

void measureTime()
{
	time_t start = clock();
	measureTimeBinary();
	time_t end = clock();
	cout << "Binary: " << double(end - start) * 1000 / CLOCKS_PER_SEC << "ms" << endl;
	start = clock();
	measureTimeAVL();
	end = clock();
	cout << "AVL: " << double(end - start) * 1000 / CLOCKS_PER_SEC << "ms" << endl;
}

void simpleTest()
{
	AVLTree<Person> temp;
	temp.insert(c);
	temp.insert(a);
	temp.insert(b);
	temp.insert(d);
	cout << temp;
}

void nameTest()
{
	AVLTree<Person, nameLess> temp;
	temp.insert(a);
	temp.insert(b);
	temp.insert(c);
	temp.insert(d);
	cout << endl << temp << endl;
}

void indexTest()
{
	AVLTree<Person> temp;
	temp.insert(a);
	temp.insert(b);
	temp.insert(c);
	temp.insert(d);
	for (int i = -1; i <= 4; ++i)
	{
		try
		{
			cout << temp[i] << endl;
		}
		catch (Exception& ex)
		{
			cout << ex << endl;
		}
	}
	cout << endl;
}

int main()
{
	simpleTest();
	nameTest();
	indexTest();
	measureTime();
	getchar();
}