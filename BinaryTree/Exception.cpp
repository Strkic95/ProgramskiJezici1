#include "Exception.h"

Exception::Exception(char* message) noexcept : message(message) {}

void Exception::print(std::ostream& str) const noexcept
{
	str << message;
}

std::ostream& operator<<(std::ostream& str, const Exception& ex)
{
	ex.print(str);
	return str;
}