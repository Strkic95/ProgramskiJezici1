#include "Person.h"

Person::Person(int index, char* name) : index(index), name(name) {}

int Person::getIndex() const
{
	return index;
}

const std::string & Person::getName() const
{
	return name;
}

bool Person::operator<(const Person& other) const
{
	return index < other.index;
}

std::ostream& operator<<(std::ostream& str, const Person& p)
{
	return str << p.index << " " << p.name;
}
