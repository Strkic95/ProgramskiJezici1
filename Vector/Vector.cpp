#include <iostream>
#include <algorithm>
#include "Vector.h"

namespace vectors
{
	Vector::Vector(int n)
	{
		arr = new double[this->n = n > 0 ? n : 1];
		std::fill(arr, arr + n, 0);
	}

	Vector::Vector(const Vector& other)
	{
		copy(other);
	}

	Vector::Vector(Vector&& other)
	{
		move(std::move(other));
	}

	Vector::~Vector() { delete[] arr; }

	Vector& Vector::operator=(const Vector& other)
	{
		if (this == &other)
			return *this;
		copy(other);
		return *this;
	}

	Vector& Vector::operator=(Vector&& other)
	{
		if (this == &other)
			return *this;
		move(std::move(other));
		return *this;
	}

	Vector Vector::operator-() const
	{
		Vector temp(*this);
		std::for_each(temp.arr, temp.arr + temp.n, [](double& d) {d = -d;});
		return temp;
	}

	Vector Vector::createEmpty(const Vector& other)
	{
		Vector temp(other.n);
		return temp;
	}

	Vector Vector::apply(const Vector& other, std::function<double(double, double)> f) const
	{
		if (n != other.n)
			return Vector(1);
		Vector temp(n);
		for (int i = 0; i < n; ++i)
			temp.arr[i] = f(arr[i], other.arr[i]);
		return temp;
	}

	void Vector::copy(const Vector& other)
	{
		arr = new double[n = other.n];
		std::copy(other.arr, other.arr + n, arr);
	}

	void Vector::move(Vector&& other)
	{
		arr = other.arr;
		n = other.n;
		other.arr = nullptr;
	}

	Vector Vector::operator+(const Vector& other) const { return apply(other, [](double a, double b) {return a + b;}); }

	Vector Vector::operator-(const Vector& other) const { return apply(other, [](double a, double b) {return a - b;}); }

	Vector Vector::operator*(const Vector& other) const { return apply(other, [](double a, double b) {return a * b;}); }

	Vector Vector::operator/(const Vector& other) const { return apply(other, [](double a, double b) {return a / b;}); }

	double Vector::operator[](int i) const
	{
		if (i < 0 || i >= n)
			return NAN;
		return arr[i]; //TODO: Make reference
	}

	bool Vector::operator==(const Vector& other) const
	{
		if (n != other.n)
			return false;
		for (int i = 0; i < n; ++i)
			if (arr[i] != other.arr[i])
				return false;
		return true;
	}

	bool Vector::operator!=(const Vector& vec) const
	{
		return !(*this == vec);
	}

	std::ostream& operator<<(std::ostream& str, const Vector& vec)
	{
		str << "(" << vec.arr[0];
		std::for_each(vec.arr + 1, vec.arr + vec.n, [&str](double& d) {str << ", " << d;});
		return str << ")";
	}

	std::istream& operator >> (std::istream& str, Vector& vec)
	{
		std::for_each(vec.arr, vec.arr + vec.n, [&str](double& d) {str >> d;});
		return str;
	}
}