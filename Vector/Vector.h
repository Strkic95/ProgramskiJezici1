#pragma once
#include <functional>
#include <iostream>

namespace vectors
{
	class Vector
	{
		friend std::ostream& operator<<(std::ostream&, const Vector&);
		friend std::istream& operator >> (std::istream&, Vector&);
	public:
		explicit Vector(int n);
		Vector(const Vector&);
		Vector(Vector&&);
		~Vector();
		Vector& operator=(const Vector&);
		Vector& operator=(Vector&&);
		Vector operator-() const;
		Vector operator+(const Vector&) const;
		Vector operator-(const Vector&) const;
		Vector operator*(const Vector&) const;
		Vector operator/(const Vector&) const;
		double operator[](int) const;
		bool operator==(const Vector&) const;
		bool operator!=(const Vector&) const;
		static Vector createEmpty(const Vector&);
	private:
		int n;
		double* arr;
		Vector apply(const Vector&, std::function<double(double, double)>) const;
		void copy(const Vector& other);
		inline void move(Vector&& other);
	};
}