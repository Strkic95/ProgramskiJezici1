#include <iostream>
#include "Vector.h"
using namespace vectors;
using std::cin;
using std::cout;

Vector memoryTest(const Vector& v, int depth)
{
	if (depth > 0)
		return memoryTest(Vector::createEmpty(v), depth - 1);
	return Vector::createEmpty(v);
}

int main()
{
	Vector temp(1000000);
	memoryTest(temp, 100);

	Vector a(3), b = Vector::createEmpty((Vector)3);
	cin >> a >> b;
	Vector c(3);
	c = a + b;
	cout << c;

	std::cin.ignore();
	std::cin.get();
}