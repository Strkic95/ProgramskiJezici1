#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <tuple>
#include <algorithm>
#include <unordered_set>

struct Point2d
{
	double x, y;
};

int main()
{
	int n;
	std::cout << "Enter the number of events: ";
	std::cin >> n;
	std::vector<std::tuple<Point2d, std::string>> events;
	for (int i = 0; i < n; ++i)
	{
		std::cout << "Event " << (i + 1) << std::endl;
		std::cout << "Enter x and y coordinates: ";
		Point2d temp;
		std::cin >> temp.x >> temp.y;
		std::cout << "Enter event name: ";
		std::string name;
		std::cin >> name;
		events.push_back(std::make_tuple(temp, name));
	}
	std::unordered_set<std::string> nameSet;
	for (auto& it : events)
		nameSet.insert(std::get<1>(it));
	std::cout << "Event names:" << std::endl;
	for (auto& it : nameSet)
		std::cout << it << std::endl;
	std::sort(events.begin(), events.end(), [](auto a, auto b) {return std::get<0>(a).x < std::get < 0>(b).x;});
	std::cout << "Events sorted by x coordinate:" << std::endl;
	//for (std::vector<std::tuple<Point2d, std::string>>::iterator& it = events.begin(); it!=events.end(); ++it)
	for (auto& it = events.begin(); it != events.end(); ++it)
		std::cout << "(" << std::get<0>(*it).x << ", " << std::get<0>(*it).y << "):" << std::get<1>(*it) << std::endl;
}